# Installing Git4edu

Git4edu consists of:

- A set of processes aimed to apply Git in general Education. Git and one, or more, graphical user interfaces (GitLab, etc)
- A Python package extending GitLab's funcionality
- A Jupyter notebook serving as a UI for everyday use of the package
- A [mailing list](https://groups.google.com/forum/#!forum/git4edu) for general communications


Please, inform any trouble you might experience to the mailing list [above](https://groups.google.com/forum/#!forum/git4edu), or see [README file](README.md)

## Running

* Activate the Python virtual environment:

```
$ cd path/to/git4edu
$ pipenv shell
```

* Run jupyter-notebook

```
(your-virtual-env)$ jupyter-notebook
```

* a web-browser should pop up, pick *Activites.ipynb*





## Installing on Linux

Components of the whole thing:

- a GitLab repository to work with, self-hosted or otherwise
- Python3, plus pipenv
- Jupyter Notebook


Optional, but recommended:

* [Pycharm](https://www.jetbrains.com/pycharm/download): complete Python IDE with Git, Jupyter and Wiki/Markdown support
* [Anaconda](https://docs.anaconda.com/anaconda/): Python distribution specialised in data analysis

Most of the above functionality is supported by GPL FLOSS, but these packages provide:
- seamless Windows, Mac, Linux environments
- ease of installation
- integration
- guidance


### apt-based Linux (Debian, Ubuntu, Huayra, etc)


* [ ] install Python3 plus [pipenv](https://docs.pipenv.org/), systemwide. Type the following:

```
apt install python3
sudo python3 -m pip install --upgrade pip
sudo pip3 install --user pipenv
```


* [ ] Install Jupyter using distro's package manager or following Jupyter [documentation](https://jupyter.readthedocs.io/en/latest/install.html).

```
sudo pip3 install jupyter
```


* [ ] Clone Git4edu's repo

```
cd ~
git clone https://gitlab.com/git4edu/gitlab.git
git clone https://gitlab.com/git4edu/gitlab.wiki.git
```

"~" can be replaced with some path of your choosing


* [ ] install the Python packages Git4edu depends on:

```
cd gitlab
pipenv install
```


Now, everything should be in place:
* [ ] System: Python3, pipenv, Jupyter
* [ ] User: git4edu virtual environment and repo files (code + [documentation](https://gitlab.com/git4edu/gitlab.wiki.git))



## Windows Install

Installing on MS Windows it's essencially the same as the Linux (reference) process, since the whole software stack is multi-platform.

 * [ ] Install Python3. You can download [the official package](https://www.python.org/downloads/windows/) or the [Acandonda distro](https://www.anaconda.com/download/#windows).

 * _note: pick any Python3 version (as opposite to Python2)_
 * _instrutions for Anaconda: [here](https://docs.anaconda.com/anaconda/install/windows)_

* [ ] Install [pipenv](https://docs.pipenv.org/) (a Python virtual environments and dependencies manager). (Not necessary with Anaconda) Open a command/"DOS" terminal and type:

```
python3 -m pip install --upgrade pip
pip3 install --user pipenv
```


* [ ] Install [Jupyter](https://jupyter.readthedocs.io/en/latest/install.html). (Not necessary with Anaconda)

```
pip3 install jupyter
```


* [ ] Download and install [Git](https://git-scm.com/download/win).



* [ ] Clone Git4edu's repo

```
cd ~
git clone https://gitlab.com/git4edu/gitlab.git
git clone https://gitlab.com/git4edu/gitlab.wiki.git
```

"~" can be replaced with some path of your choosing


* [ ] install the Python packages Git4edu depends on:

```
cd gitlab
pipenv install
```


Now, everything should be in place:
* [ ] System: Python3, pipenv, Jupyter
* [ ] User: git4edu virtual environment and repo files (code + [documentation](https://gitlab.com/git4edu/gitlab.wiki.git))


