import unittest
from pprint import pprint
import pytablewriter as twr

import git4edu as gl


class TestRepo(unittest.TestCase):

    def test___init__(self):
        result = dict(error=True, desc="")
        print("Test description: should init a connection with a gitlab repo using info on config.json or ask for it")
        try:
            self.aRepo = gl.repo()
            result["error"] = False
        except Exception as e:
            result["desc"] = e.__str__()

        print("Test result:", result)
        self.assertFalse(result["error"])

    def test_0_issues_duplicar_a_todos_notfound(self):
        self.aRepo = gl.repo()
        print("Test description: should complain about a non-existent issue")
        result = self.aRepo.issues_duplicar_a_todos(-1)
        print("Test result:", result)
        self.assertTrue(result["error"], msg="does not")
        self.assertTrue(result["desc"].lower().__contains__("not-found"), msg="Test result: wrong error message")

    def test_issues_duplicar_a_todos(self):
        self.aRepo = gl.repo()
        print("Test description: should make copies of issue #1 for every project member not in the exclude list. GitLab spam filters might apply.")
        memberExcludeList = ["eterX", "faloi", ]
        result = self.aRepo.issues_duplicar_a_todos(1, memberExcludeList=memberExcludeList)
        print("Test result:", result)
        self.assertFalse(result["error"], msg="Test result: 'error' should be False")

    def test_issues_listar(self):
        self.aRepo = gl.repo()

        for result in [
            # self.aRepo.issues_listar(),
            # self.aRepo.issues_listar(milestone=4),
            # self.aRepo.issues_listar(milestone=2,nota=True),
            # self.aRepo.issues_listar(milestone=2,nota=True),
            # self.aRepo.issues_listar(nota="fsdfsd"),
            self.aRepo.issues_listar(),
#            self.aRepo.issues_listar(nota=None),
#            self.aRepo.issues_listar(nota=10),
#            self.aRepo.issues_listar(nota=1),
            # self.aRepo.issues_listar(milestone=2,nota=10),
            # self.aRepo.issues_listar(assignee="lbatlle"),
            # self.aRepo.issues_listar(assignee="lbatlle",nota=10),
            # self.aRepo.issues_listar(assignee="lbatlle",milestone=2,nota=10),
        ]:
            pprint(pprint(result.value_matrix))
            pprint("Total de tickets en la consulta: " + str(len(result.value_matrix)))

    def test_parse_url_to_project(self):
        self.aRepo = gl.repo(connect=False)
        columnas=["test","msg","error","url","host","project_name"]
        tableOutput = twr.MarkdownTableWriter()
        tableOutput.table_name = "URLs from Repo.config[repo_url]"
        tableOutput.header_list = columnas
        tableOutput.value_matrix = list()
        tableOutput.margin = 1

        for title,URL,expectedResult in [
            ("empty string","",{"error":True,"errorDesc":"empty string"}),
            ("invalid URL", "werqerqew", {"error": True, "errorDesc": "invalid URL"}),
            ("this project","https://gitlab.com/git4edu/testProject.git",{"error":False,
                                "server_url":"https://gitlab.com","project_name":"git4edu/testProject"}),
                                ]:
            result=self.aRepo.parse_url_to_project(URL=URL)
            tableOutput.value_matrix.append([title,result["errorDesc"],
                                                 result["error"],
                                                 URL,
                                                 result["host"],
                                                 result["project_name"]])
            if result["error"]!=expectedResult["error"]:
                break
        tableOutput.write_table()
        self.assertFalse(result["error"],"see table above")


    def test_issues_validate(self):
        self.aRepo = gl.repo(connect=True,save=True)
        self.aRepo.issues_validate("..","notas.md")

    def test_issues_export(self):
        self.aRepo = gl.repo(connect=True,save=True)
        #self.aRepo = gl.repo(connect=False,save=True)
        self.aRepo.issues_export("..","notas.md")



if __name__ == "__main__":
    unittest.main()