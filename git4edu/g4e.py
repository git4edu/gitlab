import json,copy,pathlib,re  #stdlib
import gitlab  #see https://pypi.org/project/python-gitlab , Thanks to Gauvain Pocentek et al.
import pytablereader as ptr
import pytablewriter as twr
from urllib.parse import urlparse

__all__ = ['repo']
items_per_page = 1000  #config.json ...#

# issues_listar_columnas is global because is used by issues_listar ad iusses_validate
#
# the column "Ticket" is used to extrar the issues key ("iid" in GitLab jargon)
issues_listar_columnas_key="Ejercicio"
issues_listar_columnas=["Quien",issues_listar_columnas_key,"Actividad","Estado","Nota"]
#
# see https://regex101.com/r/fbUHCG/1
issue_re_str = u"(?P<antes>.*\(#)(?P<iid>\d+)(?P<despues>\).*)"  #issues <-> exercices
issue_re = re.compile(issue_re_str)
# see  https://regex101.com/r/fbUHCG/1
milestone_re_str = u"(?P<antes>.*\(%)(?P<iid>\d+)(?P<despues>\).*)"  #milestones <-> whole activities
milestone_re = re.compile(milestone_re_str)


class repo():
    def __init__(self, connect=True, save=True):
        """
        Instantiate a python-gitlab repository object, extending GitLab's API to support Git4Edu

        https://gitlab.com/git4edu/gitlab/wikis/home


        :param connect: if False, avoid connection with server, instead load saved stated. Mainly for dry-run config check and dev purposes.
        :param save: if True, save  repo state. Mainly for dev purposes.
        """
        # config.json is required to be present in the jupyter-notebooks root dir
        confPath = pathlib.Path(__file__).parent.parent
        confPath = confPath / "config.json"
        config_save_pending = False
        if confPath.exists():
            with confPath.open("r") as fh:
                self.config=json.load(fh)
        else:
            # loading generic configuration (only once if there is a successful connection)
            with pathlib.Path(__file__).parent.parent.joinpath("config-template.json").open("r") as fh:
                self.config=json.load(fh)
                config_save_pending=True #for general config changes, refreshes project ID
        ##
        ##gather server info
        ##
        if "server" not in self.config.keys():
            self.config["server"]=copy.deepcopy(self.config["00-server-default"])

        if self.config["server"]["repo_url"].startswith("default-"): #conditional is needed
            URL2=self.config["server"]["repo_url"].replace("default-","")
        else:
            URL2 = self.config["server"]["repo_url"]
        parsed=dict(error=True)
        while parsed["error"]:
            URL=input("Enter repository URL [ENTER for'{}']: ".format(URL2))
            if URL == "":
                URL=URL2
            parsed=self.parse_url_to_project(URL)
            if parsed["error"]:
                print("ERR: "+parsed["errorDesc"])

        if self.config["server"]["repo_url"]!=URL:
            self.config["server"]["repo_url"]=URL
            config_save_pending=True

        private_token=None
        while not self.private_token_is_valid(private_token):
            private_token=input("Enter server private_token [ENTER for default, 'HELP' for help]: ")
            if private_token=="HELP":
                print(self.config["00-server-help"]["private_token"])
                private_token = None
            elif private_token=="":
                print("Using saved token")
                private_token = self.config["server"]["private_token"]

        if private_token != self.config["server"]["private_token"]:
            self.config["server"]["private_token"] = private_token
            config_save_pending = True



        parsed=self.parse_url_to_project(self.config["server"]["repo_url"])

        if not connect:
            pass
            return self.load_repo()
        else:
            self.gl=gitlab.Gitlab(url=parsed["host"],
                                  private_token=self.config["server"]["private_token"],
                                  per_page = items_per_page
                                  )

            self.projects=list()
            self.projectDefaultID=None

            if config_save_pending: #see definition
                print("fetching ID for project {} from server...".format(parsed["project_name"]))
                self.project = self.gl.projects.get(parsed["project_name"])
                self.config["server"]["project_id"]=self.project.id
            else:
                self.project = self.gl.projects.get(int(self.config["server"]["project_id"])) #lazy=True?

            print("namespace/project: {}, id: {}".format(self.project.attributes["name_with_namespace"],
                                                         self.project.attributes["id"]))

            self.issues=list()
            page=[None,] #first while cycle
            pages=0
            contOK = True
            while contOK and len(page)>0:
                pages += 1
                page = self.project.issues.list(page=pages)
                self.issues+=page
                #TODO: ask user if it's OK to continue -> contOK
            print("issues: {} ({} pages)".format(len(self.issues),pages))
            self.members = self.project.members.list()
            self.milestones = self.project.milestones.list()
            if config_save_pending:
                print("Saving config to {} ... ".format(confPath.__str__()),end="")
                try:
                    with confPath.open("w") as fh:
                        fh.write(json.dumps(self.config, sort_keys=True, indent=4))
                    config_save_pending=False
                    print("saved succesfully")
                except Exception as e:
                    print("NOT saved: "+e.__str__())
            if save:
                self.save_repo()


    def save_repo(self, path=".", filename="repo-state.pickle"):
        """
        Save repo state (members, tickets, milestones).
        when "project export" function is available, use it for Backup, Recovery, Relocation, . This is mainly for dev purposes.

        :param path:
        :param filename:
        :return: {bool} successfully saved?
        """
        from pickle import Pickler

        result = False
        pathfile=pathlib.Path(path,filename)
        try:
            with open(pathfile.__str__(),mode="wb") as fh:
                Pickler(fh).dump(self)
            print("Repo (binary) state saved at "+pathfile.__str__())
            result = True
        except Exception as e:
            print("Repo (binary) state NOT saved ({})".format(e))

        return result

    def load_repo(self, path=".", filename="repo-state.pickle"):
        """
        Load repo state from repo.save() (members, tickets, milestones).
        when "project export" function is available, use it for Backup, Recovery, Relocation, . This is mainly for dev purposes.

        :param path:
        :param filename:
        :return: {None or repo()} a repo instance, if successfully loaded
        """
        from pickle import Unpickler

        result = None
        pathfile=pathlib.Path(path,filename)
        try:
            with open(pathfile.__str__(),mode="rb") as fh:
                result=Unpickler(fh).load(self)
                #Pickler(fh).dump(repoCopia)
            print("Repo (binary) state loaded from "+pathfile.__str__())
            result = True
        except Exception as e:
            print("Repo (binary) state NOT loaded ({})".format(e))

        return result


    def private_token_is_valid(self,token):
        result=False
        if type(token) == str and len(token) > 6: #good enough...
            result=True
        return result


    def parse_url_to_project(self,URL):
        error = True
        errorDesc = ""
        parsed=urlparse(URL)
        project_name=parsed.path[1:].replace(".git","")

        if parsed.hostname==None or len(project_name)!=(len(parsed.path)-5):
                errorDesc+="Empty string or invalid URL "
        else:
            error=False

        result=dict(error=error,errorDesc=errorDesc,
                    host=str(parsed.scheme)+"://"+str(parsed.hostname),
                    project_name=project_name)
        return result


    def project_get(self, repo_url):
        """


        :param repo_url: "https://gitlab.com/git4edu/gitlab.git"
        :return:  project_id: int
        """
        error=True
        errorDesc=""
        project=None
        project_name="/".join(repo_url.split("/")[-2:])[:-4]
        print("fetching ID for project {} from server...".format(project_name))
        try:
            project = self.gl.projects.get(project_name)
            print("namespace/project: {}, id: {}".format(project.attributes["name_with_namespace"],project.attributes["id"]))
            error = False
        except Exception as e:
            print("error: ",e)
            errorDesc+=e.__str__()
        return (error,errorDesc,project)

    def issues_listar(self,assignee=None,milestone=None,formato="md",nota=False,export=False):
        """
        returns an issues list, present params filter as a concatenation of AND operators.
        Bear in mind gitlab UI filters

        :param assignee: (string) username or list of usernames
        :param nota: (int) required grade (less-or-equal), or None for tickets awaiting grade
        :param milestone: (int) ID or "True" for issues not related to a milestone
        :param formato: "md" for Markdown
        :return:
        """

        if nota: #... is not False or None
            try:
                nota=int(nota) #valid for bool too
            except Exception as e:
                print("WARN: ignoring param 'nota'(grade): {}".format(e))
                nota=False

        #issues_listar_columnas now global
        result=None

        if formato=="md":
            result = twr.MarkdownTableWriter()
            result.table_name = "Issues"
            result.header_list = issues_listar_columnas
            result.value_matrix = list()
            result.margin = 1

            def formatear_md(issue):
                #global result
                xnota=None

                if issue.assignee is None:
                    #xname=issue.assignee
                    xname="None"
                else:
                    xname="{} (@{})".format(issue.assignee["name"],
                                          issue.assignee["username"])

                if False:
                    pass #relabeling affect issue_validate #TODO:validate relabeled issue states
                # if issue.state == "opened":
                #     xestado="**pendiente**"
                # elif issue.state == "closed":
                #     xestado="cerrado"
                else:
                    xestado=issue.state

                xticket=issue.title+" (#"+str(issue.iid)+")"

                if issue.milestone is None:
                    #xmilestone = None
                    xmilestone = "None"
                else:
                    xmilestone = issue.milestone["title"] + "(%" + str(issue.milestone["iid"]) + ")"
                xnota="None"
                if hasattr(issue, "labels"):
                    for label in issue.labels:
                        if label.startswith(self.config["server"]["grade_preffix"]):
                            label=label.replace(self.config["server"]["grade_preffix"],"")
                            xnota="NaN"
                            if label != "__":
                                try:
                                    xnota = int(label)
                                except Exception as e:
                                    print(e)  # TODO: error


                # All of the table result classes in pytableresult can be formatted in Jupyter Notebook table.
                result.value_matrix.append([xname,
                                            xticket,
                                            xmilestone,
                                            xestado,
                                            xnota])

            formatear=formatear_md
        else:
            raise("Valid formats: ['md',]")

        for issue in self.issues:
            filtrar_x_milestone = milestone
            filtrar_x_nota = nota
            filtrar_x_assignee = assignee

            if milestone: # ... is required and milestone >0
                if issue.milestone: #present
                    filtrar_x_milestone=issue.milestone["iid"]!=milestone
                else:
                    filtrar_x_milestone = True

            if nota is None or nota > 0:
                filtrar_x_nota = True
                grade_pending = False
                grades=list()
                if hasattr(issue,"labels"):
                    for label in issue.labels:
                        if label.startswith(self.config["server"]["grade_preffix"]):
                            try:
                                xlabel = label.replace(self.config["server"]["grade_preffix"], "")
                                if xlabel == "__":
                                    grade_pending=True
                                else:
                                    grades.append(int(xlabel))
                            except Exception as e:
                                print("ERR: issue #{} grading: {}".format(issue.iid,e))  # TODO: error

                if nota is None:
                    filtrar_x_nota = not grade_pending
                elif len(grades) > 1:
                    print("WARN: issue #{} more than one grade,  using greatest".format(str(issue.iid)))
                    filtrar_x_nota = max(grades()) > nota
                elif grades: #is not empty
                    filtrar_x_nota = grades[0] > nota

            if assignee: # ... is required and assignee !=0
                if issue.assignee: #is present
                    filtrar_x_assignee=issue.assignee["username"]!=assignee
                else:
                    filtrar_x_assignee = True



            if True not in [filtrar_x_assignee,filtrar_x_milestone,filtrar_x_nota]:
                formatear(issue)

        return result


    def issue_iid(self,field_cont="",field=""):
        """
        exports all grades to a Markdown file.
        This file could be placed in a repo (for tracking and read-only publishing)

        :param filename:
        :param path:
        :return:
        """
        result=dict(error=True,errorDesc="",iid=None)
        #issue_re global
        #issues_listar_columnas global
        if field == "milestone":
            some_re=milestone_re
        else:
            #default extract issues IID
            some_re=issue_re
        try:
            some_re_groups = some_re.findall(field_cont)
            if len(some_re_groups)==1:
                result["iid"]=int(some_re_groups[0][1])
                some_re_groups = some_re.findall(some_re_groups[0][0])
                if len(some_re_groups) > 0:
                    raise LookupError("rightmost IID is returned, more than one found")
                result["error"]=False
            else:
                raise LookupError("No IID found")
        except Exception as e:
            result["error"] = True
            result["errorDesc"]="{} in '{}'".format(e,field_cont)
        return result

    def issues_export(self, path="", filename="notas.md"):
        """
        exports all grades to a Markdown file.
        This file could be placed in a repo (for tracking and read-only publishing)

        :param filename:
        :param path:
        :return:
        """
        #issues_listar(self,assignee=None,milestone=None,formato="md",nota=False)
        grades=self.issues_listar(nota=11,export=True)
        filename=filename.replace(".md", "")+".md"
        pathfile=pathlib.Path(path,filename)
        with open(pathfile.__str__(),mode="w") as fh:
            grades.stream=fh
            grades.write_table()
            pass
        return grades

    def issue_compare(self, issue_saved, issue_current):
        """
        campare two lines of issue_listar()'s output

        :param issue_saved:
        :param issue_current:
        :return:{dict}
        """
        result=dict(same=(issue_saved == issue_current),field=None,cause="")
        if not result["same"]:
            if len(issue_saved) == len(issue_current):
                for i,saved in enumerate(issue_saved):
                    #print(i,saved,issue_current[i],saved==issue_current[i])
                    if issue_current[i]!=saved:
                        result["cause"]+="{col}:[-{current}-]-->[+{saved}+] ; ".format(col=issues_listar_columnas[i],current=issue_current[i],saved=saved)
            else:
                result["cause"]="len(saved) != len(current)"
        return result


    def issues_validate(self,path=".",filename="notas"):
        """
        Compare current grades with the saved state, as per issues_exportar() output.


        :param path:
        :param filename:
        :return:
        """
        #issues_listar_columnas global
        #issues_listar_columnas_key global
        key = issues_listar_columnas.index(issues_listar_columnas_key)
        filename=filename.replace(".md", "")+".md"
        pathfile=pathlib.Path(path) / filename
        if not pathfile.exists():
            print("ERR: {} does not exist".format(pathfile))
        else:
            grades_current=self.issues_export(path=path,filename=filename.replace(".md","-temp.md"))
            i=0
            #Index corrent state issues
            grades_current_idx=dict()
            for line in grades_current.tabledata.value_matrix:
                iid_result=self.issue_iid(line[key])
                if iid_result["error"]:
                    print("ERR: {error} in line {i} column {key}".format(error=iid_result["errorDesc"],i=i,key=key))
                if iid_result["iid"] is not None:
                    grades_current_idx[iid_result["iid"]]=i
                i+=1

            # load saved state issues
            grades_saved = ptr.MarkdownTableFileLoader(pathfile.__str__()).load()
            tablas = list()
            for tabla in grades_saved:
                tablas.append(tabla)
            if len(tablas)>1:
                print("ERR: more than one table found, using the first")

            #compare issues in saved state
            for issue_saved in tablas[0].value_matrix:
                iid_result = self.issue_iid(issue_saved[key])
                if iid_result["error"] and iid_result["iid"] is not None:
                    print("ERR: {error} in saved state, line {i} column {key}".format(error=iid_result["errorDesc"], i=i, key=key))
                else:
                    try:
                        issue_current=grades_current.tabledata.value_matrix[grades_current_idx[iid_result["iid"]]]
                        grades_current_idx.pop(iid_result["iid"])
                        # now comparing
                        compara=self.issue_compare(issue_saved, issue_current)
                        if not compara["same"]:
                            print("WARN: issue #{} differs: {}".format(iid_result["iid"],compara["cause"]))
                    except KeyError as e:
                        print("WARN: issue #{} not in **current** state".format(iid_result["iid"]))
                    except Exception as e:
                        print("WARN: issue #{}: {}".format(iid_result["iid"],e))
                i += 1
            for issue_current in grades_current_idx.keys():
                print("WARN: issue #{} not in *saved* state".format(issue_current))

    def issues_duplicar_a_todos(self,issue_iid,memberExcludeList=list()):
        """
        duplicate an issue to all the project members (1 issue per member, exceptions observed).

        :param issue_iid: ID of the original issue. example: 1 for 'https://gitlab.com/git4edu/testProject/issues/1'
        :param memberExcludeList: a list of usernames to exclude
        :return: dict  for error and error description
        """
        result=dict(error=False,desc="")
        issues_cant=0 #number of issues created
        issues_cant_excluded = 0 #users skipped
        issue_original=None
        try:
            issue_original = self.project.issues.get(issue_iid)
        except Exception as e:
            result["error"]=True
            result["desc"]=e.__str__()

        if issue_original is None:
            result["error"]=True
            result["desc"]+="; Issue #{} not-found".format(issue_iid)
        else:
            issueNuevoDict=copy.deepcopy(issue_original.attributes)
            issueKeysBorrar=('id','iid','web_url','assignees','assignee')
            for issueKeysBorrar in issueKeysBorrar:
                _=issueNuevoDict.pop(issueKeysBorrar, None )
            issueNuevoDict["milestone_id"] = issue_original.milestone["id"]
            for member in self.members:
                if member.username in memberExcludeList:
                    issues_cant_excluded += 1
                else:
                    issueNuevoDict["assignee_id"]=member.id
                    try:
                            issue = self.project.issues.create(issueNuevoDict)
                            #issue.save_repo()
                            self.issues.append(issue)
                            issues_cant += 1
                    #except  GitlabCreateError as e: # TODO: manage <class gitlab.exceptions.GitlabCreateError>  errors
                    except  Exception as e:
                        print("ERR: {}; assignee: {} ".format(e,member.username))
                        result["error"] = True
                        result["desc"]+="Error creating issue for {username}({name}) ;".format(username=member.username,name=member.name)

            if issues_cant == 0:
                result["error"]=True
            result["desc"]+="; {} Issues created".format(issues_cant)
            result["desc"]+="; {} Users excluded ".format(issues_cant_excluded)

        return result


if __name__ == '__main__':
    print("""
this module is not meant to be run directly
try these jupyter-notebooks: Activities.ipnb
""")
