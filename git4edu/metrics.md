## g4e.py - raw metrics:
    LOC: 541
    LLOC: 349
    SLOC: 361
    Comments: 58
    Single comments: 35
    Multi: 65
    Blank: 80
    - Comment Stats
        (C % L): 11%
        (C % S): 16%
        (C + M % L): 23%
## g4e.py - Cyclomatic Complexity:
    M 25:4 repo.__init__ - D
    M 228:4 repo.issues_listar - C
    M 428:4 repo.issues_validate - C
    M 486:4 repo.issues_duplicar_a_todos - B
    C 24:0 repo - B
    M 357:4 repo.issue_iid - A
    M 408:4 repo.issue_compare - A
    M 135:4 repo.save_repo - A
    M 158:4 repo.load_repo - A
    M 183:4 repo.private_token_is_valid - A
    M 190:4 repo.parse_url_to_project - A
    M 207:4 repo.project_get - A
    M 389:4 repo.issues_export - A
## g4e.py - Maintainability Index: 
    A