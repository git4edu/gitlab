# Installing Git4edu

Git4edu consists of:

- A set of prosseses for not-so specialliced teachers, aimed to apply Git to general education, coding included. Git and one or more graphical user interfaces (GitLab)
- A Python package extending GitLab's funcionality
- A Jupyter notebook serving as a UI for everyday use of the package


Please, see [README file](README.md)

## Running

* Activate the Python virtual enviroment:

```
$ cd path/to/git4edu
$ pipenv shell
```

* Run jupyter-notebook

```
(virtual-env)$ jupyter-notebook
```

* open a web-browser (according to instructions on screen) and pick *Activites.ipynb*




## Installing on Linux

Dependencies:

- GitLab repository, self-hosted or Internet
- Python3, plus pipenv
- Jupyter Notebook

### apt/Debian-based Linux (Ubuntu, Huayra, etc)


* [ ] install python3

```
$ apt install python3
```


* [ ] Install Jupyter using distro's package manager or following Jupyter [documentation](https://jupyter.readthedocs.io/en/latest/install.html).

```
$ python3 -m pip install --upgrade pip
$ python3 -m pip install jupyter
```


* [ ] Install pipenv  - https://docs.pipenv.org/

```
$ pip3 install --user pipenv
```



* [ ] clone Git4edu's repo

```
$ git clone https://gitlab.com/git4edu/gitlab.git
```





