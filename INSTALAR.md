# Instalando Git4edu

Git4edu consiste en:

- Un conjunto de procesos para docentes no-especializados, orientados a la aplicación de Git a educación en general, incluso la Programación. Git y una interfaz gráfica, o mas.
- Un paquete de Python que extiende la funcionalidad de GitLab
- Un cuaderno  Jupyter que aporta una interfaz gráfica para el uso diario de ese paquete


Por favor, vea el [archivo README](README.md)

## Ejecución

* Active el entorno virtual de Python:

```
$ cd path/to/git4edu
$ pipenv shell
```

* Corra el servidor de jupyter-notebook

```
(virtual-env)$ jupyter-notebook
```

* abra un navegador web (siguiendo las instrucicones en pantalla) y elija *Activites.ipynb*




## Instalando en Linux

Dependencias:

- Repositorio de GitLab, propio o en  Internet
- Python3, con pipenv
- Jupyter Notebook

### Linux basado en apt/Debian (Ubuntu, Huayra, etc)


* [ ] instalar python3

```
$ apt install python3
```


* [ ] Instalar Jupyter con el manejador de paquetes de la distribución o siguiendo la [documentación](https://jupyter.readthedocs.io/en/latest/install.html) de Jupyter.

```
$ python3 -m pip install --upgrade pip
$ python3 -m pip install jupyter
```


* [ ] Instalar pipenv  - https://docs.pipenv.org/

```
$ pip3 install --user pipenv
```



* [ ] clonar el repo de Git4edu

```
$ git clone https://gitlab.com/git4edu/gitlab.git
```






## Instalando en Windows

La instalación en  MS Windows es esencialmente la misma que en Linux, ya que todo el software es multi-plataforma.

 * [ ] La instalación de  Python3. Se puede descargar de [el paquete oficial](https://www.python.org/downloads/windows/) or the [distribución de Acandonda](https://www.anaconda.com/download/#windows).

 * _nota: tomar alguna versión de Python3 (en oposición a Python2)_
 * _instrucciones para Anaconda: [aquí](https://docs.anaconda.com/anaconda/install/windows)_

* [ ] Instalar [pipenv](https://docs.pipenv.org/) (un administrador de entornos virtuales y dependencias de Python). (No es necesario con Anaconda) Abrir una línea de comando/terminal "DOS" y tipear:

```
python3 -m pip install --upgrade pip
pip3 install --user pipenv
```


* [ ] Instalar [Jupyter](https://jupyter.readthedocs.io/en/latest/install.html). (No es necesario con Anaconda)

```
pip3 install jupyter
```


* [ ] Descargar e instalar [Git](https://git-scm.com/download/win).



* [ ] Clonar el repositorio de Git4edu

```
cd ~
git clone https://gitlab.com/git4edu/gitlab.git
git clone https://gitlab.com/git4edu/gitlab.wiki.git
```

"~" puede ser reemplazado con algún path a elección


* [ ] instalar los paquetes de  Python para Git4edu:

```
cd gitlab
pipenv install
```


Ahora, todo debería estar en su lugar:
* [ ] Sistema: Python3, pipenv, Jupyter
* [ ] Usuario: git4edu virtual environment and repo files (code + [documentation](https://gitlab.com/git4edu/gitlab.wiki.git))

