# Git for Education 

Git4edu consists of:

- A set of processes aimed to apply Git in general Education. Git and one, or more, graphical user interfaces (GitLab, etc)
- A Python package extending GitLab's funcionality
- A Jupyter notebook serving as a UI for everyday use of the package
- A [mailing list](https://groups.google.com/forum/#!forum/git4edu) for general communications


## Git para la Educación
 
- [Versión en Castellano](https://gitlab.com/git4edu/gitlab/wikis/es/procesos)
- [Charla relámpago en **Nerdear.la**](https://www.youtube.com/watch?v=TN_Dx74efDo)
 
 
##  Git para a Educação

 [Versão em português](https://gitlab.com/git4edu/gitlab/wikis/home)
 
 
## Intro / Motivation 
 
This project is both the result of years of using Git and GitLab in education, and an attempt to supply non-technical teachers with tools to leverage issue tracking systems in everyday (didactic) activities.


## Options


See README.md


## Pros & cons

See README.md


## Gitlab
 
 This proyect is mainly focused in processes and development based on GitLab, but we are not affiliated neither with GitLab nor [GitHub Education](https://education.github.com/). 

## Licence

 Code is covered by the [GNU GPLv3](https://gitlab.com/git4edu/gitlab/blob/master/LICENSE) license.
 All other content, including processes, are released under the Creative Commons [Attribution-ShareAlike 2.5 Argentina](https://creativecommons.org/licenses/by-sa/2.5/ar/deed.en) license
