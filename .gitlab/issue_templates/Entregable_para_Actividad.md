
```
Revisar antes de copiar:
```

/title (prioridad a,b,c,etc) Nuevo Título
/milestone %_
/label ~"To Do" ~Nota___
/due
ticket raíz al final

### Enunciado

Qué hay que hacer


### Objetivos:

1. Objetivo S.M.A.R.T.
1. Objetivo S.M.A.R.T.



### Pasos:

1. [ ] primer paso
1. [ ] segundo paso
1. [ ] tercer paso



---
### Verificación


Antes de cerrar éste *ticket*,  agregue un comentario abajo con una captura de pantalla. Arrastrando  el archivo o con el botón ![image](/uploads/00d9b2ca16a5895c1d3c099d639e03b6/image.png). Luego puede cerrar éste ticket y recibirá un mail con la corrección.

---
ticket raíz: #_

